# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2013 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    'name': 'KM Sistemas - Project task automatic sequence module',
    'version': '1.0',
    'category': 'Others',
    'description': """
This module assigns an automatic sequence number to every new task in a project.
It adds a new tree view which only shows not cancelled or done tasks, ordered
by sequence, that allows to change sequence by clicking 'move up/down' buttons.

When you click up/down buttons in this view, by default OpenERP Gtk client does
NOT refresh the view, which turns into incosistent data until 'Reload' button is
clicked.

Here is the solution (you have to edit a file from the Gtk client, no way, but it's worth it):

1.  Find the file openerp-client/widget/view/list.py and locate __contextual_menu method.
    
2.  Apply these changes (also found on 'list.patch' file on the root of the git repository):

@@ -587,7 +587,24 @@
                                self.screen.display()
                                return False
                         id = current_active_model.id
-                        current_active_model.get_button_action(self.screen, id, path[1].attrs)                        
+                        current_active_model.get_button_action(self.screen, id, path[1].attrs)
+                        
+                        # Este desarrollo posibilita (sólo en el cliente GTK, no en el web) que, si se define un botón
+                        # dentro de un tree con {'reload': True} dentro del contexto, cada vez que se pulse el botón, después
+                        # de ejecutar su acción se pulsa el botón Reload de la pantalla de OpenERP. Esto se ha desarrollado
+                        # únicamente para modificar el campo 'sequence' de los registros del tree a través de dos botones de
+                        # subir/bajar. Como no sabemos cómo se comportaría este código si el botón tiene otro tipo de acción
+                        # distinta de las que hemos programado para modificar el sequence (por ejemplo, la apertura de otra
+                        # ventana), hemos hecho que la acción solamente se ejecute si 'reload': True viaja en el contexto del
+                        # botón.
+                        if 'context' in path[1].attrs:
+                            # String to dict. Source: http://stackoverflow.com/questions/988228/converting-a-string-to-dictionary
+                            import ast
+                            button_context = ast.literal_eval(path[1].attrs['context'])
+                            if 'reload' in button_context and button_context.get('reload', False):
+                                # Force button click. Source: http://stackoverflow.com/questions/13667394/how-do-i-manually-trigger-an-event-with-pygtk-event-handler
+                                service.LocalService('gui.main').buttons['but_reload'].emit("clicked")    
+                        
                         self.screen.current_model = None
                         if self.screen.parent and isinstance(self.screen.parent, ModelRecord):           
                             self.screen.parent.reload()

3.  Save and make sure to close Gtk client and restart it.

To initially reorder all existent tasks after module installation, simply close and reopen any task in progress.

NOTE: With this change to list.py file, you'll be able to force the 'Reload' button click of the main form when clicking a
tree view button, as long as you declare this button in the tree view XML with {'reload': True} in its context.

    """,
    'author': 'KM Sistemas de información, S.L.',
    'website': 'http://www.kmsistemas.com',
    'license': 'AGPL-3',
    'depends': [
        'base',
        'project',
    ],
    'update_xml': [
        'project_view.xml',
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
#    'certificate': 'certificate',
}