#Project task sequence
This module assigns an automatic sequence number to every new task in a project.
It adds a new tree view which only shows not cancelled or done tasks, ordered
by sequence, that allows to change sequence by clicking 'move up/down' buttons.

Watch it working on [this video](https://vimeo.com/81704696).
(you might not be able to see it on Chrome; try another browser)

When you click up/down buttons in this view, by default OpenERP Gtk client does
NOT refresh the view, which turns into incosistent data until 'Reload' button is
clicked.

Here is the solution (you have to edit a file from the Gtk client, no way, but it's worth it):

1.  Find the file openerp-client/widget/view/list.py and locate __contextual_menu method.
    
2.  Apply the changes included in the 'list.patch' file.

3.  Save and make sure to close Gtk client and restart it.

To initially reorder all existent tasks after module installation, simply close and reopen any task in progress.

NOTE: With this change to list.py file, you'll be able to force the 'Reload' button click of the main form when clicking a
tree view button, as long as you declare this button in the tree view XML with {'reload': True} in its context.