# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2013 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import osv,fields
from openerp.tools.translate import _




class task(osv.Model):

    
    _inherit = 'project.task'
    
    
    def _get_default_sequence(self, cr, uid, context = {}):
        cr.execute("""SELECT max(sequence) FROM project_task WHERE state NOT IN ('done','cancelled')""")
        data = cr.fetchone()
        max_active_seq = data and data[0] or 0
        res = max_active_seq + 1
        return res
    
    
    _columns = {
        'sequence': fields.integer('Seq', readonly=True, select=True, help="Gives the sequence order when displaying a list of tasks."),
    }
    
    _defaults = {  
        'sequence': _get_default_sequence,
    }

    _order = 'sequence' 
    
    
    def resequence_open_tasks(self,cr,uid,context={}):
        task_ids = self.search(cr, uid, [('state', 'not in', ('done','cancelled'))], order='sequence,priority,date_start')
        for i, id in enumerate(task_ids):
            cr.execute("""UPDATE project_task SET sequence = %s WHERE id = %s""" % (i + 1, id))
        return True
    
    
    def do_close(self, cr, uid, ids, context={}):
        res = super(task,self).do_close(cr, uid, ids, context=context)
        cr.execute("""UPDATE project_task SET sequence = -1 WHERE id IN %s""", (tuple(ids),))
        self.resequence_open_tasks(cr,uid)
        return res
    
    
    def do_cancel(self, cr, uid, ids, context={}):
        res = super(task,self).do_cancel(cr, uid, ids, context=context)
        cr.execute("""UPDATE project_task SET sequence = -1 WHERE id IN %s""", (tuple(ids),))
        self.resequence_open_tasks(cr,uid)
        return res
    
    
    def do_reopen(self, cr, uid, ids, context={}):
        res = super(task,self).do_reopen(cr, uid, ids, context=context)
        for id in ids:
            seq = self._get_default_sequence(cr, uid, context)
            cr.execute("""UPDATE project_task SET sequence = %s WHERE id = %s""" % (seq, id))
        self.resequence_open_tasks(cr,uid)
        return res
    
    
    def sequence_incr(self, cr, uid, ids, context = {}):
        # Aumenta la secuencia: decrece la prioridad
        assert len(ids) == 1, 'Only one task can be moved at a time'
        id = ids and ids[0] or False
        if id:
            current_task = self.browse(cr,uid,id,context=context)
            current_task_seq = current_task.sequence
            # Get the next sequence's task id
            cr.execute("""SELECT id, sequence FROM project_task WHERE state NOT IN ('done','cancelled') AND sequence > %s ORDER BY sequence LIMIT 1;""" % current_task_seq)
            data = cr.fetchone()
            if data and len(data) == 2:
                next_task_id = data[0]
                next_task_seq = data[1]
                # Exchange sequences
                cr.execute("""UPDATE project_task SET sequence = %s WHERE id = %s""" % (next_task_seq, current_task.id))
                cr.execute("""UPDATE project_task SET sequence = %s WHERE id = %s""" % (current_task_seq, next_task_id))
        return True


    def sequence_decr(self, cr, uid, ids, context = None):
        # Decrece la secuencia: aumenta la prioridad
        assert len(ids) == 1, 'Only one task can be moved at a time'
        id = ids and ids[0] or False
        if id:
            current_task = self.browse(cr,uid,id,context=context)
            current_task_seq = current_task.sequence
            # Get the next sequence's task id
            cr.execute("""SELECT id, sequence FROM project_task WHERE state NOT IN ('done','cancelled') AND sequence < %s ORDER BY sequence desc LIMIT 1;""" % current_task_seq)
            data = cr.fetchone()
            if data and len(data) == 2:
                prev_task_id = data[0]
                prev_task_seq = data[1]
                # Exchange sequences
                cr.execute("""UPDATE project_task SET sequence = %s WHERE id = %s""" % (prev_task_seq, current_task.id))
                cr.execute("""UPDATE project_task SET sequence = %s WHERE id = %s""" % (current_task_seq, prev_task_id))
        return True

task()